import hopps.client

import tornado.ioloop


async def main():
    await hopps.client.HoppsConnection('ws://localhost:5919').connect()

if __name__ == '__main__':
    tornado.ioloop.IOLoop.current().run_sync(main)
