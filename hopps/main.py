"""
Usage: hopps [--username=<username>]
             [--hostname=<hostname>]
             [--port=<port>]
             [--mongodb=<hostname>]
             [--admin]

-h --help               show this
--username=<username>   drop permissions to the given user
--hostname=<hostname>   host to listen on [default: localhost]
--port=<port>           port to listen on [default: 5919]
--mongodb=<hostname>    host to connect to [default: localhost:27017]
--admin                 bypass authentication checks, but only allow
                        user modification.
"""

import logging
import os
import pwd

import docopt
import pypledge
import tornado.ioloop

import hopps

logger = logging.getLogger(__name__)


def lockdown(username: str) -> None:
    user_info = pwd.getpwnam(username)
    gid = int(user_info.pw_gid)
    uid = int(user_info.pw_uid)

    os.chroot('/var/empty')
    os.chdir('/')

    os.setgroups([])
    os.setgid(gid)
    os.setuid(uid)


def main():
    options = docopt.docopt(__doc__)
    username = options['--username']
    hostname = options['--hostname']
    port = int(options['--port'])
    mongodb_hostname = options['--mongodb']
    admin_mode = bool(options['--admin'])

    logging.basicConfig(level=logging.INFO)

    connection = hopps.Connection('vetshelf', mongodb_hostname)
    tornado.ioloop.IOLoop.current().run_sync(connection.initialize)

    application = tornado.web.Application([
        (r'/', hopps.HoppsHandler)
    ], conn=connection, admin_mode=admin_mode)
    application.listen(port, address=hostname)

    if username:
        try:
            lockdown(username)
        except PermissionError:
            logger.warning('Failed to drop permissions')
    else:
        logger.warning('Not dropping permissions')

    try:
        pypledge.pledge(['stdio', 'inet'])
    except (PermissionError, OSError) as err:
        logger.warning(err)

    if admin_mode:
        logger.warning('Starting in user admin mode')

    tornado.ioloop.IOLoop.instance().start()
