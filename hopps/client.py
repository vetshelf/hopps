import collections
from typing import Dict, List

import tornado.websocket


class HoppsConnection:
    def __init__(self, host: str) -> None:
        self.message_id = 0
        self.host = host
        self.conn = None  # type: tornado.websocket.WebSocketClientConnection
        self.pending = {}
        self.messages = collections.deque()

    async def connect(self) -> 'HoppsConnection':
        self.conn = await tornado.websocket.websocket_connect(self.host,
                                                              on_message_callback=self._on_message)
        return self

    async def save(self, collection: str, doc: Dict[str, object]) -> None:
        await self.__send({'save': [collection, doc]})

    async def watch(self) -> None:
        await self.__send({'watch': []})

    async def dump(self, collection: str) -> None:
        await self.__send({'dump': [collection]})

    async def auth(self, username: str, password: str) -> None:
        await self.__send({'auth': [username, password]})

    async def create_user(self, username: str, password: str, roles: List[str]) -> None:
        await self.__send({'create-user': [username, password, roles]})

    async def revoke_user(self, username: str) -> None:
        await self.__send({'revoke-user': [username]})

    async def list_users(self) -> None:
        await self.__send({'list-users': []})

    async def __send(self, msg: Dict[str, object]):
        self.message_id += 1
        msg['i'] = self.message_id
        await self.conn.write_message(json.dumps(msg))

    def _on_message(self, message: str) -> None:
        if message is None:

            self.on_close()
        else:
            self.on_message(message)

    def on_close(self) -> None:
        self.messages.append(None)

    def on_message(self, message: str) -> None:
        self.messages.append(message)

    def pop(self) -> str:
        return self.messages.popleft()
