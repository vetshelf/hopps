"""
Usage: hopps-cli [<uri>]

-h --help   show this
<uri>       hopps server to connect to [default: ws://localhost:5919]
"""

try:
    import readline
except ImportError:
    pass

import collections
import concurrent
import json
import logging
import shlex
from typing import Dict, List

import docopt
import tornado.concurrent
import tornado.gen
import tornado.ioloop
import tornado.websocket

import hopps.client

logger = logging.getLogger(__name__)


class StopException(Exception):
    pass


class ClosedException(StopException):
    pass


class Client:
    executor = concurrent.futures.ThreadPoolExecutor()

    def __init__(self, host: str) -> None:
        self.host = host
        self.conn = None  # type: hopps.client.HoppsConnection

    async def handle_help(self):
        """help()"""
        props = [p for p in dir(self) if p.startswith('handle_')]
        for attr_name in props:
            attr = getattr(self, attr_name)
            print('  {}'.format(attr.__doc__))

    async def handle_quit(self):
        """quit()"""
        raise StopException()

    async def handle_exit(self):
        """exit()"""
        raise StopException()

    async def handle_save(self, collection, raw_doc: str):
        """save(collection, doc)"""
        doc = json.loads(raw_doc)
        await self.conn.save(collection, doc)

    async def handle_watch(self):
        """watch()"""
        await self.conn.watch()

    async def handle_dump(self, collection: str):
        """dump()"""
        await self.conn.dump(collection)

    async def handle_auth(self, username: str, password: str):
        """auth(username, password)"""
        await self.conn.auth(username, password)

    async def handle_create_user(self, username: str, password: str, roles: str):
        """create_user(username, password, roles)"""
        await self.conn.create_user(username, password, roles.split())

    async def handle_revoke_user(self, username: str):
        """revoke_user(username)"""
        await self.conn.revoke_user(username)

    async def handle_list_users(self):
        """list_users()"""
        await self.conn.list_users()

    @tornado.gen.coroutine
    def _prompt(self):
        while self.conn.messages:
            message = self.conn.pop()
            if message is None:
                raise ClosedException()

            print('  remote> {}'.format(message))

        raw_stanza = yield self._input()
        parts = shlex.split(raw_stanza)
        if not parts:
            return

        command = parts[0]
        try:
            yield getattr(self, 'handle_{}'.format(command))(*parts[1:])
        except AttributeError:
            logger.error('Unknown method "%s"', command)
        except TypeError as err:
            logger.error(err)

    async def mainloop(self):
        self.conn = await hopps.client.HoppsConnection(self.host).connect()

        try:
            while True:
                await self._prompt()
        except ClosedException:
            print('Host closed connection')
        except StopException:
            pass
        except KeyboardInterrupt:
            pass
        except EOFError:
            print('')

    @tornado.concurrent.run_on_executor
    def _input(self):
        return input('> ')


def main():
    options = docopt.docopt(__doc__)
    host = str(dict(options).get('uri', 'ws://localhost:5919'))
    logging.basicConfig(level=logging.WARNING)

    client = Client(host)
    tornado.ioloop.IOLoop.current().run_sync(client.mainloop)
