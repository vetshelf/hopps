TESTS=$(wildcard tests/*.py)
TESTS_FINISHED=$(TESTS:.py=.done)

.PHONY: lint test clean

all: lint $(TESTS_FINISHED)

lint:
	pep8 --max-line-length=100 hopps tests

tests/%.done: tests/%.py
	python $<
	touch $@

clean:
	rm -f tests/*.done
