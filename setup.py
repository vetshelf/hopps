from setuptools import setup

setup(
    name='hopps',
    version='0.0.0',
    author="Andrew Aldridge",
    author_email="i80and@foxquill.com",
    license='AGPL3',
    packages=['hopps'],
    install_requires=['docopt', 'motor', 'tornado', 'pypledge', 'argon2'],
    classifiers=[
        'Programming Language :: Python :: 3',
        ],
    entry_points={
        'console_scripts': [
            'hoppsd = hopps.main:main',
            'hopps-cli = hopps.cli:main'
            ],
        }
)
